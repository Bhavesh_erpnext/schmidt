# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in schmidt/__init__.py
from schmidt import __version__ as version

setup(
	name='schmidt',
	version=version,
	description='Customization',
	author='Bhavesh',
	author_email='erpnextdeveloper1@gmail.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)

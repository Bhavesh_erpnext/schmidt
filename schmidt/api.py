from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.utils import cint,flt
from frappe.sessions import Session, clear_sessions, delete_session
from frappe import _
import random
import string
from frappe.utils import get_link_to_form

@frappe.whitelist()
def add_field_in_doctype(label,fieldtype,doctype):
	
	doc = frappe.get_doc(dict(
		doctype = "DocField",
		lebel = label,
		fieldtype = fieldtype,
		parent = doctype,
		parenttype="DocType",
		parentfield = "fields"
	)).insert(ignore_permissions = True)

def get_project_list(doctype, txt, searchfield, start, page_len, filters):
	#if frappe.session.user == "Administrator":
	return frappe.db.sql("""select name from `tabSchmidt Project` where exists (select name from `tabJob` where project=`tabSchmidt Project`.name)""")

def get_job_list(doctype, txt, searchfield, start, page_len, filters):
	# if project == None or project == '':
	# 	frappe.throw("Select Project First")
	return frappe.db.sql("""select name from `tabJob` where dispatched=1 and status <> 'Completed'""")

@frappe.whitelist()
def after_user_insert(self,method):
	#frappe.msgprint('Call')
	if self.name == 'Administartor' or self.name == 'administrator':
		frappe.throw("Can't change for this user")
	if self.custom_user_type == "Employee":
		frappe.db.sql("""delete from `tabHas Role` where parent=%s""",self.name)
		frappe.db.sql("""INSERT INTO `tabHas Role` (`name`, `creation`, `modified`, `modified_by`, `owner`, `docstatus`, `parent`, `parentfield`, `parenttype`, `idx`, `role`) VALUES ('"""+id_generator(10)+"""', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '"""+self.name+"""', '"""+self.name+"""', '0', '"""+self.name+"""', 'roles', 'User', '1', 'Employee')""")
	if self.custom_user_type == "Manager":
		frappe.db.sql("""delete from `tabHas Role` where parent=%s""",self.name)
		frappe.db.sql("""INSERT INTO `tabHas Role` (`name`, `creation`, `modified`, `modified_by`, `owner`, `docstatus`, `parent`, `parentfield`, `parenttype`, `idx`, `role`) VALUES ('"""+id_generator(10)+"""', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '"""+self.name+"""', '"""+self.name+"""', '0', '"""+self.name+"""', 'roles', 'User', '1', 'Schmidt Manager')""")

def id_generator(size):
   return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(size))

@frappe.whitelist()
def check_duplicate_time_tracker(project,employee):
	data = frappe.db.sql("""select name,project from `tabTime Tracker` where project=%s and employee=%s""",(project,employee),as_dict=1)
	if len(data) >= 1:
		return data
	else:
		return False

@frappe.whitelist()
def send_email_test():
	arabic_template="email_html.html"
	ar_recipients = ['erpnextdeveloper1@gmail.com']
	frappe.sendmail(recipients=ar_recipients, subject=_("Test From File"),
	message=frappe.get_template(arabic_template).render({}),
	delayed=True)

@frappe.whitelist()
def create_auto_customer_job(self,method):
	create_customer(self)
	create_job(self)
	create_customer_user(self)
	add_webform_url(self)

def add_webform_url(self):
	if self.doctype == "Exterior Estimate":
		frappe.db.set_value(self.doctype,self.name,"webform_url","https://easycontractingsolutions.com/exterior-estimate?name=")

@frappe.whitelist()
def create_customer(self):
	doc = frappe.get_doc(dict(
		doctype = "Client",
		customer_name = self.customer_name,
		email = self.email,
		mobile = self.mobile_no,
		address_line1 = self.address,
		state = self.state,
		city = self.city,
		zip = self.zip
	)).insert(ignore_permissions = True)
	frappe.db.set_value(self.doctype,self.name,"customer",doc.name)

@frappe.whitelist()
def create_job(self):
	doc  = frappe.get_doc(dict(
		doctype = "Job",
		job_type = "Fixed Job",
		estimation = self.name,
		description = '',
		invoice_item = get_item(self.doctype),
		estimate= get_estimate_type(self.doctype),
		address_line1 = self.address,
		estimate_doctype = self.doctype
	)).insert(ignore_permissions = True)
	frappe.db.set_value(self.doctype,self.name,"first_job",doc.name)

def get_item(doctype):
	if doctype == "Exterior Estimate":
		return "External Painting"
	if doctype == "Interior Estimate":
		return "Internal Painting"
	if doctype == "Power Wash Estimate":
		return "Power Wash"

@frappe.whitelist()
def create_ph_job(doctype,name):
	doc = frappe.get_doc(doctype,name)
	# title = ''
	# if doc.name:
	# 	title += self.name
	# if doc.address_line1:
	# 	title += '-'+str(doc.address_line1)
	first_ph = check_first_ph_job(doc.name)
	job_doc  = frappe.get_doc(dict(
		doctype = "Job",
		job_type = "PerHour Job",
		estimation = doc.name,
		description ='',
		invoice_item = 'Other Services',
		estimate= get_estimate_type(doc.doctype),
		address_line1 = doc.address,
		project = doc.project,
		estimate_doctype = doc.doctype,
		first_ph_job = first_ph
	)).insert(ignore_permissions = True)
	#add_job_in_estimate(job_doc.name,job_doc.job_type,doctype,name)
	msg = '<a href="#Form/Job/'+job_doc.name+'" target="_blank">'+job_doc.name+'</a>'
	frappe.msgprint(_("Job {0} created").format(msg))

def check_first_ph_job(est):
	check_ph_job = frappe.db.sql("""select name from `tabJob` where job_type='PerHour Job' and estimation=%s""",est)
	if len(check_ph_job) >= 1:
		return 0
	else:
		return 1

@frappe.whitelist()
def create_project(doctype,name,booked=None):
	if not booked == None:
		doc = frappe.get_doc(doctype,name)
		project_doc = frappe.get_doc(dict(
			doctype = "Schmidt Project",
			customer = doc.customer,
			customer_name = doc.customer_name,
			email = doc.email,
			mobile = doc.mobile_no,
			address_line1 = doc.address,
			state = doc.state,
			city = doc.city,
			zip = doc.zip,
			estimate = get_estimate_type(doc.doctype),
			estimation = doc.name,
			estimate_doctype = doc.doctype,
			estimate_item = get_item(doc.doctype)
		)).insert(ignore_permissions = True)
		if project_doc:
			job_data = frappe.get_all("Job",filters={"estimation":doc.name},fields=["name","job_type"],order_by="creation")
			for row in job_data:
				jobs = project_doc.append("estimate_job",{})
				jobs.job = row.name
				jobs.job_type = row.job_type
				frappe.db.set_value("Job",row.name,"project",project_doc.name)
			project_doc.save()
			frappe.db.set_value(doctype,name,"status","Booked")
			frappe.msgprint(_("Project {0} created").format(get_link_to_form("Schmidt Project", project_doc.name)))
	else:
		frappe.db.set_value(doctype,name,"status","Not Booked")


def get_estimate_type(doctype):
	if doctype == "Exterior Estimate":
		return "EXT"
	if doctype == "Interior Estimate":
		return "INT"
	if doctype == "Power Wash Estimate":
		return "PW"

@frappe.whitelist()
def add_job_in_estimate(self,method):
	estimate = frappe.get_doc(self.estimate_doctype,self.estimation)
	doc = frappe.get_doc(dict(
		doctype = "Estimate Job",
		parentfield = "estimate_job",
		parent = estimate.name,
		parenttype = self.estimate_doctype,
		job = self.name,
		job_type = self.job_type,
		job_description = self.description,
		hourly_rate = self.hour_rate,
		estimate_hours = self.estimate_hours,
		idx = len(estimate.estimate_job) + 1
	)).insert(ignore_permissions = True)
	if self.project:
		add_job_in_project(self.project,self.name,self.job_type)


@frappe.whitelist()
def add_job_in_project(project,job_name,job_type):
	project_data = frappe.get_doc("Schmidt Project",project)
	doc = frappe.get_doc(dict(
		doctype = "Estimate Job",
		parentfield = "estimate_job",
		parent = project,
		parenttype = "Schmidt Project",
		job = job_name,
		job_type = job_type,
		job_description = self.description,
		hourly_rate = self.hour_rate,
		idx = len(project_data.estimate_job) + 1
	)).insert(ignore_permissions = True)

@frappe.whitelist()
def update_job_from_estimate(name,job,description=None,rate=None,estimate_hours=None):
	frappe.db.set_value("Job",job,"description",description)
	frappe.db.set_value("Estimate Job",name,"job_description",description)
	if estimate_hours and flt(estimate_hours) > 0:
		frappe.db.set_value("Job",job,"estimate_hours",estimate_hours)
	if rate and flt(rate) > 0:
		frappe.db.set_value("Job",job,"hour_rate",rate)
		frappe.db.set_value("Estimate Job",name,"hourly_rate",rate)
		first_ph_job = frappe.db.get_value("Job",job,"first_ph_job")
		if int(first_ph_job) == 1:
			update_estimate_data(job)
	
	return True

@frappe.whitelist()
def create_customer_user(self):
	from frappe.utils.password import update_password
	doc = frappe.get_doc(dict(
		doctype = "User",
		custom_user_type = "Customer",
		email = self.email,
		first_name = self.customer_name,
		send_welcome_email = 0
	)).insert(ignore_permissions = True)
	update_password(doc.name,str(self.mobile_no))
	frappe.db.set_value(self.doctype,self.name,"user",doc.name)

@frappe.whitelist()
def assign_per_hour_data_to_estimate(self,method):
	if self.first_ph_job:
		if int(self.first_ph_job) == 1:
			frappe.db.set_value(self.estimate_doctype,self.estimation,"job_description",self.description)
			frappe.db.set_value(self.estimate_doctype,self.estimation,"perhour_rate",self.hour_rate)
			frappe.db.set_value(self.estimate_doctype,self.estimation,"estimate_hours",self.estimate_hours)

def update_estimate_data(job):
	doc = frappe.get_doc("Job",job)
	frappe.db.set_value(doc.estimate_doctype,doc.estimation,"job_description",doc.description)
	frappe.db.set_value(doc.estimate_doctype,doc.estimation,"perhour_rate",doc.hour_rate)
	frappe.db.set_value(doc.estimate_doctype,doc.estimation,"estimate_hours",doc.estimate_hours)

@frappe.whitelist()
def update_send_email(est):
	doc = frappe.get_doc("Exterior Estimate",est)
	doc.send_email = int(doc.send_email) + 1
	doc.save()
	user = frappe.db.get_value("Exterior Estimate",est,"user")
	if user:
		frappe.db.set_value("Exterior Estimate",est,"owner",user)


@frappe.whitelist()
def create_webform(doctype):
	fieldtypes = (frappe.get_meta('Web Form Field').get_field("fieldtype").options or "").split("\n")
	meta = frappe.get_meta(doctype)
	doc = frappe.get_doc("DocType",doctype)
	fields = []
	for f in meta.fields:
		if not f.fieldtype in fieldtypes:
			msg = f.fieldtype + " FieldType Not Supported For Form"
			frappe.throw(msg)
		field = dict(
			fieldname = f.fieldname,
			label = f.label,
			fieldtype = f.fieldtype,
			options = f.options,
			reqd = f.reqd,
			default = f.default,
			read_only = f.read_only,
			depends_on = f.depends_on,
			hidden = f.hidden,
			description = f.description
		)
		fields.append(field)

	webform_doc = frappe.get_doc(dict(
		doctype = "Web Form",
		title = doc.form_title,
		route = doc.form_route,
		doc_type = doctype,
		module = doc.module,
		web_form_fields = fields,
		success_url = doc.success_url,
		published = 1,
		login_required = 1,
		allow_edit = 1
	)).insert(ignore_permissions = True)
	if webform_doc:
		frappe.db.set_value("DocType",doctype,"form_builder",webform_doc.name)
		return webform_doc.name
	else:
		return False

@frappe.whitelist()
def update_doctype_form_field(self,method):
	if self.doc_type:
		frappe.db.set_value("DocType",self.doc_type,"form_builder","")


# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "schmidt"
app_title = "Schmidt"
app_publisher = "Bhavesh"
app_description = "Customization"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "erpnextdeveloper1@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/schmidt/css/schmidt.css"
# app_include_js = "/assets/schmidt/js/schmidt.js"

# include js, css files in header of web template
# web_include_css = "/assets/schmidt/css/schmidt.css"
# web_include_js = "/assets/schmidt/js/schmidt.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "schmidt.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "schmidt.install.before_install"
# after_install = "schmidt.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "schmidt.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

doc_events = {
	"User": {
		"on_update": "schmidt.api.after_user_insert"
	},
	("Exterior Estimate","Power Wash Estimate","Interior Estimate"): {
		"after_insert":"schmidt.api.create_auto_customer_job"
	},
	"Job":{
		"after_insert":"schmidt.api.add_job_in_estimate",
		"on_update":"schmidt.api.assign_per_hour_data_to_estimate"
	},
	"Invoice":{
		"on_submit":"schmidt.schmidt.doctype.quickbooks_settings.quickbooks_settings.sync_sales_invoice_in_qb"
	},
	"Web Form":{
		"after_delete":"schmidt.api.update_doctype_form_field"
	}
}

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"schmidt.tasks.all"
# 	],
# 	"daily": [
# 		"schmidt.tasks.daily"
# 	],
# 	"hourly": [
# 		"schmidt.tasks.hourly"
# 	],
# 	"weekly": [
# 		"schmidt.tasks.weekly"
# 	]
# 	"monthly": [
# 		"schmidt.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "schmidt.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "schmidt.event.get_events"
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "schmidt.task.get_dashboard_data"
# }


// Copyright (c) 2020, Bhavesh and contributors
// For license information, please see license.txt

frappe.ui.form.on('Interior Estimate', {
	refresh: function(frm) {
		frm.add_custom_button(__("Reload"), function() {
			cur_frm.reload_doc();
		})
		if(!frm.doc.__islocal){
			frm.add_custom_button(__("Create PerHour Job"), function() {
				frappe.call({
					method:"schmidt.api.create_ph_job",
					args:{'doctype':frm.doc.doctype,'name':frm.doc.name},
					callback:function(r){
	
					}
				})
			})
			if(frm.doc.status == "Open"){
			frm.add_custom_button(__("Close"), function() {
				frappe.confirm(
					'Jobs Booked?',
					function(){
						frappe.call({
							method:"schmidt.api.create_project",
							args:{'doctype':frm.doc.doctype,'name':frm.doc.name,'booked':'Yes'},
							async:false,
							callback:function(r){
								cur_frm.reload_doc()
							}
						})
						window.close();
					},
					function(){
						frappe.call({
							method:"schmidt.api.create_project",
							args:{'doctype':frm.doc.doctype,'name':frm.doc.name},
							async:false,
							callback:function(r){
								cur_frm.reload_doc()
							}
						})
						window.close();
					}
				)
	
				cur_frm.reload_doc()
			})
		}
	
		}
			// if(!frm.doc.__islocal && !frm.doc.customer){
			// 	frm.add_custom_button(__("Create Customer"), function() {
			// 			frappe.route_options = {"customer_name": frm.doc.customer_name,"email":frm.doc.email,"mobile":frm.doc.mobile_no,"address_line1":frm.doc.address,"state":frm.doc.state,"city":frm.doc.city,"zip":frm.doc.zip,"estimate_id":frm.doc.name};
			// 			frappe.set_route("Form", "Client", "New Client 1")
			// 		})
			// 	}
				// if(!frm.doc.__islocal && frm.doc.customer){
				// 	frm.add_custom_button(__("Create Project"), function() {
				// 			frappe.route_options = {"customer":frm.doc.customer,"customer_name": frm.doc.customer_name,"email":frm.doc.email,"mobile":frm.doc.mobile_no,"address_line1":frm.doc.address_line1,"state":frm.doc.state,"city":frm.doc.city,"zip":frm.doc.zip,"estimate_id":frm.doc.name};
				// 			frappe.set_route("Form", "Schmidt Project", "New Schmidt Project 1")
				// 		})
				// 	}
	},
	after_save:function(frm){
		frappe.timeout(0.4);
		cur_frm.reload_doc();
	}
});

frappe.ui.form.on('Estimate Job', {
	update:function(frm,cdt,cdn) {
		var doc = locals[cdt][cdn];
		console.log(frm)
		if(doc.job){
			frappe.call({
				method:"schmidt.api.update_job_from_estimate",
				args:{'name':doc.name,'job':doc.job,'description':doc.job_description,'rate':doc.hourly_rate},
				callback:function(r){
					if(r.message == true){
						show_alert("Job Updated");
						cur_frm.reload_doc();
					}
				}
			})
		}
	}

})
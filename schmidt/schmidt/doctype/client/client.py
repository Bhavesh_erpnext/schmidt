# -*- coding: utf-8 -*-
# Copyright (c) 2020, Bhavesh and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Client(Document):
	def on_update(self):
		if self.estimate_id:
			frappe.db.set_value("Exterior Estimate",self.estimate_id,"customer",self.name)

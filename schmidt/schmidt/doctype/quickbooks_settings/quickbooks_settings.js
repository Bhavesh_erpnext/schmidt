// Copyright (c) 2020, Bhavesh and contributors
// For license information, please see license.txt

frappe.provide("frappe.ui.form");
frappe.provide("Quickbooks Settings");

frappe.ui.form.on('Quickbooks Settings', {
	refresh: function(frm) {
	var me = this;
	var quickbooks_authentication_url = ""
	 }
});
cur_frm.cscript.connect_qb = function () {
	var me = this;
	if((cur_frm.doc.client_key != null) && (cur_frm.doc.client_secret != null) && (cur_frm.doc.client_key.trim() != "") && (cur_frm.doc.client_secret.trim() != "")){
		return frappe.call({
				method: "schmidt.schmidt.doctype.quickbooks_settings.quickbooks_settings.quickbooks_authentication_popup",
				args:{ 
					"consumer_key": cur_frm.doc.client_key,
					"consumer_secret": cur_frm.doc.client_secret},
				freeze: true,
	 			freeze_message:"Please wait.. connecting to Quickbooks ................",
				callback: function(r) {
					if(r.message){
						window.location.assign(r.message)
						// pop_up_window(decodeURIComponent(r.message),"Quickbooks");
					}
				}
		});
	}else{
		msgprint(__("Please Enter Proper Consumer Key and Consumer Secret"));
	}
},
cur_frm.cscript.sync_customer = function () {
	frappe.call({
		method:"schmidt.schmidt.doctype.quickbooks_settings.quickbooks_settings.sync_customer",
		args:{},
		callback:function(r){
			if(r.message){
				console.log(r)
			}
		}
	})
}
// pop_up_window = function(url,windowName) {
// 	window.location.assign(url)
// }
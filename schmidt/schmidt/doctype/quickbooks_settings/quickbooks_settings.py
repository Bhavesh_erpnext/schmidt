# -*- coding: utf-8 -*-
# Copyright (c) 2020, Bhavesh and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe import _,msgprint
from frappe.utils import cstr, flt, cint, get_files_path
from intuitlib.client import AuthClient
from intuitlib.enums import Scopes
from quickbooks.client import QuickBooks, Environments
import json
import requests
from frappe.utils import flt, nowdate

class QuickbooksSettings(Document):
	pass

 

@frappe.whitelist(allow_guest=True)
def First_callback(realmId,code):
	login_via_oauth2(realmId,code)
	frappe.local.response["type"] = "redirect"
	frappe.local.response["location"] = "/desk#Form/Quickbooks Settings"

def login_via_oauth2(realmId,code):
	""" Store necessary token's to Setup service """
	
	quickbooks_settings = frappe.get_doc("Quickbooks Settings")
	quickbooks_settings.auth_code = code
	quickbooks_settings.realm_id = realmId
	auth_client = AuthClient(
		client_id=quickbooks_settings.client_key,
		client_secret=quickbooks_settings.client_secret,
		environment = "production",
		redirect_uri='https://easycontractingsolutions.com/api/method/schmidt.schmidt.doctype.quickbooks_settings.quickbooks_settings.First_callback'
	)
	auth_client.get_bearer_token(quickbooks_settings.auth_code, realm_id=quickbooks_settings.realm_id)
	quickbooks_settings.access_token = auth_client.access_token
	quickbooks_settings.refresh_token = auth_client.refresh_token
	quickbooks_settings.save()
	frappe.db.commit()

@frappe.whitelist(allow_guest=True)
def quickbooks_authentication_popup(consumer_key, consumer_secret):
	""" Open new popup window to Connect Quickbooks App to Quickbooks sandbox Account """

	quickbooks_settings = frappe.get_doc("Quickbooks Settings")
	auth_client = AuthClient(
		client_id=consumer_key,
		client_secret=consumer_secret,
		environment = "production",
		redirect_uri='https://easycontractingsolutions.com/api/method/schmidt.schmidt.doctype.quickbooks_settings.quickbooks_settings.First_callback'
	)
	auth_url = auth_client.get_authorization_url([Scopes.ACCOUNTING])

	# quickbooks = QuickBooks(
    #        sandbox=True,
    #        consumer_key = quickbooks_settings.client_key,
    #        consumer_secret = quickbooks_settings.client_secret,
    #        callback_url = 'https://easycontractingsolutions.com/api/method/erpnext_quickbooks.erpnext_quickbooks.doctype.quickbooks_settings.quickbooks_settings.First_callback'
    # )

	return auth_url

@frappe.whitelist()
def sync_customer_qb():
	quickbooks_settings = frappe.get_doc("Quickbooks Settings")
	auth_client = AuthClient(
		client_id=quickbooks_settings.client_key,
		client_secret=quickbooks_settings.client_secret,
		environment = "production",
		redirect_uri='https://easycontractingsolutions.com/api/method/schmidt.schmidt.doctype.quickbooks_settings.quickbooks_settings.First_callback'
	)
	from quickbooks.objects.customer import Customer
	client = QuickBooks(
	auth_client=auth_client,
	refresh_token=quickbooks_settings.refresh_token,
	company_id=quickbooks_settings.realm_id
	)
	customer_obj = Customer()
	customer_obj.FullyQualifiedName = 'Test1122'
	customer_obj.DisplayName = 'Test1122'
	res = customer_obj.save(qb=client) 
	return res

@frappe.whitelist()
def sync_sales_invoice_in_qb(self,method):
	from quickbooks.objects.detailline import SalesItemLine, SalesItemLineDetail
	from quickbooks.objects.invoice import Invoice
	quickbooks_settings = frappe.get_doc("Quickbooks Settings")
	auth_client = AuthClient(
		client_id=quickbooks_settings.client_key,
		client_secret=quickbooks_settings.client_secret,
		environment = "production",
		redirect_uri='https://easycontractingsolutions.com/api/method/schmidt.schmidt.doctype.quickbooks_settings.quickbooks_settings.First_callback'
	)
	client = QuickBooks(
	auth_client=auth_client,
	refresh_token=quickbooks_settings.refresh_token,
	company_id=quickbooks_settings.realm_id
	)
	sync_customer_qb(self,client)
	cust_qb_id = frappe.db.get_value("Client",self.customer,"quickbooks_id")
	if not cust_qb_id:
		frappe.throw("Customer Not Able To Sync")
	invoice = Invoice()
	invoice.DocNumber = self.name
	for row in self.invoice_item:
		sync_item_qb(row.item,client,row.description)
		item_qb_id = frappe.db.get_value("Job Item",row.item,"quickbooks_id")
		if not item_qb_id:
			frappe.throw("Item Not Able To Sync")
		line = SalesItemLine()
		line.LineNum = row.idx
		line.Description = row.description
		line.Amount = row.amount
		line.SalesItemLineDetail = SalesItemLineDetail()
		line.SalesItemLineDetail.Qty = row.qty
		line.SalesItemLineDetail.UnitPrice = row.rate
		line.SalesItemLineDetail.ItemRef = {"value": item_qb_id, "name":row.item}
		invoice.Line.append(line)
	invoice.CustomerRef = {"value":cust_qb_id}
	invoice.save(qb=client)
	frappe.db.set_value(self.doctype,self.name,"quickbooks_id",invoice.Id)
	frappe.db.set_value(self.doctype,self.name,"sync",1)

def sync_item_qb(item_name,client,item_des=None):
	item_details = frappe.get_doc("Job Item",item_name)
	if int(item_details.sync) == 0:
		from quickbooks.objects.item import Item
		item = Item()
		item.Name = item_name
		item.Description = item_des if item_des else item_name
		item.Type = "Service"
		item.ExpenseAccountRef = {"value": "28", "name": "Cost of sales"}
		item.IncomeAccountRef = {"value": "21", "name": "Sales of Product Income"}
		item.save(qb=client)
		frappe.db.sql("""Update `tabJob Item` set sync=1,quickbooks_id=%s""",(item.Id))


@frappe.whitelist()
def sync_customer_qb(self,client):
	client_doc = frappe.get_doc("Client",self.customer)
	if int(client_doc.sync) == 0:
		from quickbooks.objects.customer import Customer
		from quickbooks.objects.base import Address
		customer_obj = Customer()
		customer_obj.FullyQualifiedName = self.customer_name
		customer_obj.DisplayName = self.customer_name
		customer_obj.BillAddr = Address()
		customer_obj.BillAddr.Line1 = client_doc.address_line1
		customer_obj.BillAddr.City = client_doc.city
		customer_obj.BillAddr.Country = client_doc.state
		customer_obj.BillAddr.PostalCode = client_doc.zip
		customer_obj.save(qb=client)
		#frappe.msgprint(customer_obj.Id)
		frappe.db.set_value("Client",self.customer,"sync",1)
		frappe.db.set_value("Client",self.customer,"quickbooks_id",customer_obj.Id)
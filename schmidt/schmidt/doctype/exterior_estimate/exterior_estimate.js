// Copyright (c) 2020, Bhavesh and contributors
// For license information, please see license.txt

frappe.ui.form.on('Exterior Estimate', {
	refresh: function(frm) {
		frm.add_custom_button(__("Reload"), function() {
			cur_frm.reload_doc();
		})
		if(!frm.doc.__islocal){
		frm.add_custom_button(__("Create PerHour Job"), function() {
			frappe.call({
				method:"schmidt.api.create_ph_job",
				args:{'doctype':frm.doc.doctype,'name':frm.doc.name},
				callback:function(r){
					cur_frm.reload_doc()
				}
			})
		})
		frm.add_custom_button(__("Send Email"), function() {
			if(!frm.doc.total){
				frappe.throw("Total Is Mandatory")
			}
			// if(!frm.doc.amount_due_upon_start){
			// 	frappe.throw("Amount Due Upon Start Is Mandatory")
			// }
			// if(!frm.doc.amount_due_upon_start){
			// 	frappe.throw("Amount Due Upon Finish Is Mandatory")
			// }
			if(!frm.doc.upfront_payment){
				frappe.throw("Upfront Payment Rate Is Mandatory")
			}
			if(!frm.doc.number_of_installment){
				frappe.throw("Number of Installments For Remaining Amount Is Mandatory")
			}
			frappe.call({
				method:"schmidt.api.update_send_email",
				args:{'est':frm.doc.name},
				callback:function(r){
					cur_frm.reload_doc()
					frappe.msgprint("Email Sent")
				}
			})
		})
		if(frm.doc.status == "Open"){
		frm.add_custom_button(__("Close"), function() {
			frappe.confirm(
				'Jobs Booked?',
				function(){
					frappe.call({
						method:"schmidt.api.create_project",
						args:{'doctype':frm.doc.doctype,'name':frm.doc.name,'booked':'Yes'},
						async:false,
						callback:function(r){
							cur_frm.reload_doc()
						}
					})
					window.close();
				},
				function(){
					frappe.call({
						method:"schmidt.api.create_project",
						args:{'doctype':frm.doc.doctype,'name':frm.doc.name},
						async:false,
						callback:function(r){
							cur_frm.reload_doc()
						}
					})
					window.close();
				}
			)

			cur_frm.reload_doc()
		})
	}

	}
		// if(!frm.doc.__islocal && !frm.doc.customer){
		// 	frm.add_custom_button(__("Create Customer"), function() {
		// 			frappe.route_options = {"customer_name": frm.doc.customer_name,"email":frm.doc.email,"mobile":frm.doc.mobile_no,"address_line1":frm.doc.address,"state":frm.doc.state,"city":frm.doc.city,"zip":frm.doc.zip,"estimate_id":frm.doc.name};
		// 			frappe.set_route("Form", "Client", "New Client 1")
		// 		})
		// 	}
			// if(!frm.doc.__islocal && frm.doc.customer){
			// 	frm.add_custom_button(__("Create Project"), function() {
			// 			frappe.route_options = {"customer":frm.doc.customer,"customer_name": frm.doc.customer_name,"email":frm.doc.email,"mobile":frm.doc.mobile_no,"address_line1":frm.doc.address_line1,"state":frm.doc.state,"city":frm.doc.city,"zip":frm.doc.zip,"estimate_id":frm.doc.name};
			// 			frappe.set_route("Form", "Schmidt Project", "New Schmidt Project 1")
			// 		})
			// 	}
			

	},
	after_save:function(frm){
		frappe.timeout(0.4);
		cur_frm.reload_doc();
	},
	number_of_installment:function(frm,cdt,cdn){
		var doc = locals[cdt][cdn];
		var remain_amount = (parseFloat(doc.total) - parseFloat((parseFloat(doc.total)*parseFloat(doc.upfront_payment))/100)) / parseFloat(doc.number_of_installment) 
		frappe.model.set_value(cdt,cdn,"installment_amount",remain_amount)
	}
});
frappe.ui.form.on('Estimate Job', {
	update:function(frm,cdt,cdn) {
		var doc = locals[cdt][cdn];
		console.log(frm)
		if(doc.job){
			frappe.call({
				method:"schmidt.api.update_job_from_estimate",
				args:{'name':doc.name,'job':doc.job,'description':doc.job_description,'rate':doc.hourly_rate,'estimate_hours':doc.estimate_hours},
				callback:function(r){
					if(r.message == true){
						show_alert("Job Updated")
					}
				}
			})
		}
	}

})
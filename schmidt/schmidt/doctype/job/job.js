// Copyright (c) 2020, Bhavesh and contributors
// For license information, please see license.txt

frappe.ui.form.on('Job', {
	onload:function(frm){
		if(frm.doc.__islocal){
			frm.add_child("employee_project_assign")
		}
	},
	refresh: function(frm) {


		if(!frm.doc.__islocal && frm.doc.status != "Completed"){
			frm.add_custom_button(__("Complete Job"), function() {
				frappe.call({
					method:"schmidt.schmidt.doctype.job.job.complete_job",
					args:{'name':frm.doc.name},
					callback:function(r){
						if(r.message){
							cur_frm.reload_doc();
						}
					}
				})
			});
		}
	}
});

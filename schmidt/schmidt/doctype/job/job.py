# -*- coding: utf-8 -*-
# Copyright (c) 2020, Bhavesh and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.model.naming import make_autoname
from frappe.utils import flt, nowdate, get_url,cstr
from six.moves.urllib.parse import urlencode
from datetime import timedelta

class Job(Document):
	def autoname(self):
		if self.job_type == "PerHour Job":
			prefix = 'J-PH-'+str(self.estimate) + '-.####.-' + str(self.address_line1)
			self.name = make_autoname(prefix)
		else:
			prefix = 'J-'+str(self.estimate) + '-.####.-' + str(self.address_line1)
			self.name = make_autoname(prefix)

	def validate(self):
		self.validate_dispatch()
	
	def validate_dispatch(self):
		if len(self.employee_project_assign) >= 1:
			self.dispatched = 1
		else:
			self.dispatched = 0

	def after_insert(self):
		update_assign_job_dispatch(self.employee_project_assign)

	def on_update(self):
		update_assign_job_dispatch(self.employee_project_assign)

@frappe.whitelist()
def complete_job(name):
	frappe.db.set_value("Job",name,"status","Completed")
	total_hour = calculate_total_hour(name)
	if total_hour:
		frappe.errprint(total_hour)
		total_hour_format = str(timedelta(minutes=flt(total_hour)*60)).split(':')
		frappe.errprint(total_hour_format)
		total_hour_format_str = cstr(total_hour_format[0])+':'+cstr(total_hour_format[1])
		frappe.db.sql("Job",name,"total_hours_format",cstr(total_hour_format_str))
		frappe.db.set_value("Job",name,"total_hours",total_hour)
	return True

def update_assign_job_dispatch(dispatch_job):
	for row in dispatch_job:
		doc = frappe.get_doc(row.doctype,row.name)
		if not int(doc.dispatch) == 1:
			doc.dispatch = 1
			res = create_time_tracker(row.parent,row.employee)
			doc.time_tracker_url = res
			doc.save()
			send_message(doc,row.employee)

def create_time_tracker(job,employee):
	tt_doc = frappe.get_doc(dict(
		doctype = "Time Tracker",
		project = job,
		employee = employee
	)).insert(ignore_permissions = True)
	url = get_url("desk#Form/Time%20Tracker/{0}".format(tt_doc.name))
	return url


def calculate_total_hour(job):
	data = frappe.db.sql("""select sum(total_billable_hours) from `tabTime Tracker` where project=%s""",job)
	if len(data) >= 1:
		return data[0][0]
	else:
		return 0

@frappe.whitelist()
def send_message(doc,employee):
	try:
		from twilio.rest import Client
		mobile_no = frappe.db.get_value("Employee",employee,"cell_number")
		if mobile_no:
			msg = 'Hi, {0} \nYou were dispatched {1} to the Time Tracker. The job was scheduled on {3}.\nThank You.\n Schmidt Painting'.format(doc.get('employee_name'),doc.get('time_tracker_url'),doc.get('parent'),doc.get('start_date'))
			twilio_setting = frappe.get_doc("Twilio Setting","Twilio Setting")
			client = Client(twilio_setting.account_sid, twilio_setting.auth_token) 
			message = client.messages.create(from_=twilio_setting.from_number,body=msg,to=mobile_no)
			if message:
				msg_log = frappe.get_doc(dict(
					doctype = "Twilio Message Log",
					from_no = twilio_setting.from_number,
					to = mobile_no,
					message = msg,
					time = nowdate()
				)).insert(ignore_permissions = True)
	except Exception:
		frappe.log_error(frappe.get_traceback())
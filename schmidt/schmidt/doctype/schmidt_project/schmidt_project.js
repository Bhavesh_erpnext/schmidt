// Copyright (c) 2020, Bhavesh and contributors
// For license information, please see license.txt

frappe.ui.form.on('Schmidt Project', {
	refresh: function(frm) {
		$(".title-text").css("max-width", "100%");
		$(".title-text").css("font-size", "20px"); 
		if(!frm.doc.__islocal && frm.doc.status != "Completed"){
		frm.add_custom_button(__("Create Job"), function() {
				frappe.route_options = {"project": frm.doc.name ,"estimate_doctype":frm.doc.estimate_doctype,"invoice_item":frm.doc.estimate_item,"estimation":frm.doc.estimation};
				frappe.set_route("Form", "Job", "New Job 1")
			})
			frm.add_custom_button(__("Complete"), function() {
				frappe.call({
					method:"schmidt.schmidt.doctype.schmidt_project.schmidt_project.complete_project",
					args:{'project':frm.doc.name},
					callback:function(r){
						if(r.message){
							cur_frm.reload_doc()
						}
					}
				})
			})
		}
		if(!frm.doc.__islocal && frm.doc.status == "Completed"){
			frm.add_custom_button(__("Make Invoice"), function() {
				frappe.call({
					method:"schmidt.schmidt.doctype.schmidt_project.schmidt_project.make_invoice",
					args:{'project':frm.doc.name},
					callback:function(r){
						if(r.message){
							cur_frm.reload_doc()
							frappe.set_route("Form","Invoice",r.message)
						}
					}
				})
			})

		}
	}
});

# -*- coding: utf-8 -*-
# Copyright (c) 2020, Bhavesh and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.model.naming import make_autoname


class SchmidtProject(Document):
	def autoname(self):
		prefix = 'P-' + str(self.estimate) + '-.#####.-' + str(self.customer_name) + '-' + str(self.address_line1)
		self.name = make_autoname(prefix)
		self.project_name = self.name

@frappe.whitelist()
def complete_project(project):
	frappe.db.set_value("Schmidt Project",project,"status","Completed")
	return True

@frappe.whitelist()
def make_invoice(project):
	customer = frappe.db.get_value("Schmidt Project",project,"customer")
	job_list = frappe.get_all("Job",filters={'project':project},fields=["*"])
	if len(job_list) >= 1:
		items = []
		for row in job_list:
			if row.job_type == "Fixed Job":
				item = dict(
					item = row.invoice_item,
					qty = 1,
					rate = row.fixed_amount,
					job = row.name
				)
				items.append(item)
			if row.job_type == "PerHour Job":
				item = dict(
					item = row.invoice_item,
					qty = row.total_hours,
					rate = row.hour_rate,
					job = row.name
				)
				items.append(item)
		res = create_invoice(items,customer,project)
		frappe.db.set_value("Schmidt Project",project,"sales_invoice",res.name)
		return res.name
	else:
		return False

def create_invoice(items,customer,project):
	doc = frappe.get_doc(dict(
		doctype = "Invoice",
		customer = customer,
		invoice_item = items,
		project= project
	)).insert()
	return doc
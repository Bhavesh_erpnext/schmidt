// Copyright (c) 2020, Bhavesh and contributors
// For license information, please see license.txt

// frappe.ui.form.on('Time Tracker', {
// 	// refresh: function(frm) {

// 	// }
// });


frappe.ui.form.on('Time Tracker', {
	setup:function(frm){
		// frm.set_query('job', function() {
		// 	return {
		// 		filters: {
		// 			'project': frm.doc.project
		// 		}
		// 	}
		// });

	},
	onload:function(){
		console.log('asd')
		frappe.hide_msgprint();
		$("#navbar-breadcrumbs").hide()
	},
	refresh: function(frm) {
		frappe.flags.pause_job = 0;
		frappe.flags.resume_job = 0;
		frappe.flags.complete_job = 0;

		// if(!frm.doc.__islocal && frm.doc.items && frm.doc.items.length) {
		// 	if (frm.doc.for_quantity != frm.doc.transferred_qty) {
		// 		frm.add_custom_button(__("Material Request"), () => {
		// 			frm.trigger("make_material_request");
		// 		});
		// 	}

		// 	if (frm.doc.for_quantity != frm.doc.transferred_qty) {
		// 		frm.add_custom_button(__("Material Transfer"), () => {
		// 			frm.trigger("make_stock_entry");
		// 		}).addClass("btn-primary");
		// 	}
		// }
		// $("#start").click(function(){
		// 	alert();
		// })

		
			frm.trigger("prepare_timer_buttons");
			$(".c-msg").click(function(){
				frappe.hide_msgprint();
			})

		
	},
	// before_save:function(frm){
	// 	if(frm.doc.project && frm.doc.employee){
	// 		frappe.call({
	// 			method:"schmidt.api.check_duplicate_time_tracker",
	// 			args:{'project':frm.doc.project,'employee':frm.doc.employee},
	// 			callback:function(r){
	// 				if(r.message != false)
	// 				{
	// 					var msg = 'Time Tracker Available For :'+'<a href="#Form/Time Tracker/'+r.message[0].name+'" class="c-msg">'+r.message[0].project+'</a>'
	// 					frappe.throw(msg)
	// 				}
	// 			}
	// 		})
	// 	}

	// },
	validate:function(frm){

		frm.doc.total_time_in_mins = 0;
		frm.doc.time_logs.forEach(d => {
			if (d.time_in_mins) {
				frm.doc.total_time_in_mins += d.time_in_mins;
			}
		});
		frm.doc.total_billable_hours = frm.doc.total_time_in_mins/60
		frm.doc.total_billable_amount = frm.doc.total_billable_hours * frm.doc.hourly_rate
		refresh_field("total_time_in_mins");
		refresh_field("total_billable_hours");
		refresh_field("total_billable_amount");
		setTimeout(function(){ cur_frm.reload_doc() },1500);

	},

	prepare_timer_buttons: function(frm) {
		frm.trigger("make_dashboard");
		console.log(frm.doc.job_started)
		if(frm.doc.status == "Completed"){
			
			$("#paush").hide()
			$("#resume").hide()
			$("#complete").hide()
		}
		if (frm.doc.job_started == 1) {
			
		}
		else{
			$("#paush").hide()
			$("#resume").hide()
			$("#complete").hide()
		}
		if(frm.doc.status == "On Hold"){
			$("#start").hide()
			$("#paush").hide()
		}
		if(frm.doc.status == "Work In Progress"){
			$("#start").hide()
			$("#resume").hide()
		}


		$("#paush").click(function(){
			console.log('paush')
			frappe.flags.pause_job = 1;
			frm.set_value("status", "On Hold");
			frm.events.complete_job(frm);
		})
		$("#resume").click(function(){
			frappe.flags.resume_job = 1;
			frm.events.start_job(frm);
		})

		$("#complete").click(function(){
			frappe.flags.complete_job = 1;
			let completed_time = frappe.datetime.now_datetime();
			frm.trigger("hide_timer");
			frm.events.complete_job(frm, completed_time);

		})
		$("#start").click(function(){
			if (!frm.doc.employee) {
				frappe.prompt({fieldtype: 'Link', label: __('Employee'), options: "Employee",
					fieldname: 'employee'}, d => {
					if (d.employee) {
						frm.set_value("employee", d.employee);
					}

					frm.events.start_job(frm);
				}, __("Enter Value"), __("Start"));
			} else {
				frm.events.start_job(frm);
			}

		}).addClass("btn-primary");
		// if (!frm.doc.job_started) {
		// 	$("#start").click(function(){
		// 		if (!frm.doc.employee) {
		// 			frappe.prompt({fieldtype: 'Link', label: __('Employee'), options: "Employee",
		// 				fieldname: 'employee'}, d => {
		// 				if (d.employee) {
		// 					frm.set_value("employee", d.employee);
		// 				}

		// 				frm.events.start_job(frm);
		// 			}, __("Enter Value"), __("Start"));
		// 		} else {
		// 			frm.events.start_job(frm);
		// 		}
		// 	}).addClass("btn-primary");

		// } else if (frm.doc.status == "On Hold") {
		// 	frm.add_custom_button(__("Resume"), () => {
		// 		frappe.flags.resume_job = 1;
		// 		frm.events.start_job(frm);
		// 	}).addClass("btn-primary");
		// 	frm.add_custom_button(__("Complete"), () => {
		// 		let completed_time = frappe.datetime.now_datetime();
		// 		frm.trigger("hide_timer");
		// 		frm.events.complete_job(frm, completed_time);
		// 	}).addClass("btn-primary");
		// } else {
		// 	// frm.add_custom_button(__("Pause"), () => {
		// 	// 	frappe.flags.pause_job = 1;
		// 	// 	frm.set_value("status", "On Hold");
		// 	// 	frm.events.complete_job(frm);
		// 	// }).addClass("btn-primary");

		// 	frm.add_custom_button(__("Complete"), () => {
		// 		let completed_time = frappe.datetime.now_datetime();
		// 		frm.trigger("hide_timer");
		// 		frm.events.complete_job(frm, completed_time);
		// 	}).addClass("btn-primary");
		// }
	},

	start_job: function(frm) {
		let row = frappe.model.add_child(frm.doc, 'Job Card Time Log', 'time_logs');
		row.from_time_format = frappe.datetime.now_datetime();
		row.from_time = frappe.datetime.now_datetime();
		navigator.geolocation.getCurrentPosition(success);
		var lat = '';
		var long = '';
		function success(pos) {
			console.log('asd')
			var crd = pos.coords;
			row.clockin_latitude = crd.latitude;
			row.clockin_longitude = crd.longitude;
		}
		frm.set_value('job_started', 1);
		frm.set_value('started_time' , row.from_time);
		frm.set_value("status", "Work In Progress");
		console.log('start')
		console.log(frm)
		if (!frappe.flags.resume_job) {
			frm.set_value('current_time' , 0);
		}

		frm.save();
	},

	complete_job: function(frm, completed_time) {
		console.log('call')
		frm.doc.time_logs.forEach(d => {
			if (d.from_time && !d.to_time) {
				console.log('in')
				d.to_time_format = completed_time || frappe.datetime.now_datetime();
				d.to_time = completed_time || frappe.datetime.now_datetime();
				d.time_in_mins = moment(d.to_time).diff(moment(d.from_time),"seconds")/60 || 0;
				d.time_in_hour = moment(d.to_time).diff(moment(d.from_time),"seconds")/3600 || 0;
				navigator.geolocation.getCurrentPosition(success);
				var lat = '';
				var long = '';
				function success(pos) {
					console.log('asd')
					var crd = pos.coords;
					d.clockout_latitude = crd.latitude;
					d.clockout_longitude = crd.longitude;
;
				}

				if(frappe.flags.pause_job) {
					let currentIncrement = moment(d.to_time).diff(moment(d.from_time),"seconds") || 0;
					frm.set_value('current_time' , currentIncrement + (frm.doc.current_time || 0));
				} else {
					frm.set_value('started_time' , '');
					//frm.set_value('job_started', 0);
					frm.set_value('current_time' , 0);
				}
				if(frappe.flags.complete_job){
					frm.set_value("status","Completed")
				}
				frm.save();
			}
			else{
				if(frappe.flags.complete_job){
					frm.set_value("status","Completed")
				}
				frm.set_value('started_time' , '');
				//frm.set_value('job_started', 0);
				frm.set_value('current_time' , 0);
				frm.save();
			}
		});
		//cur_frm.reload_doc();
		
	},

	make_dashboard: function(frm) {
		if(frm.doc.__islocal)
			return;

		frm.dashboard.refresh();
		const timer = `
			<div class="stopwatch" style="font-weight:bold;
				color:#545454;font-size:40px;display:inline-block;vertical-align:text-bottom;>
				<span class="hours">00</span>
				<span class="colon">:</span>
				<span class="minutes">00</span>
				<span class="colon">:</span>
				<span class="seconds">00</span>
			</div>`;

		var section = frm.toolbar.page.add_inner_message(timer);

		let currentIncrement = frm.doc.current_time || 0;
		if (frm.doc.started_time || frm.doc.current_time) {
			if (frm.doc.status == "On Hold") {
				updateStopwatch(currentIncrement);
			} else {
				currentIncrement += moment(frappe.datetime.now_datetime()).diff(moment(frm.doc.started_time),"seconds");
				initialiseTimer();
			}

			function initialiseTimer() {
				const interval = setInterval(function() {
					var current = setCurrentIncrement();
					updateStopwatch(current);
				}, 1000);
			}
	
			function updateStopwatch(increment) {
				var hours = Math.floor(increment / 3600);
				var minutes = Math.floor((increment - (hours * 3600)) / 60);
				var seconds = increment - (hours * 3600) - (minutes * 60);
	
				$(section).find(".hours").text(hours < 10 ? ("0" + hours.toString()) : hours.toString());
				$(section).find(".minutes").text(minutes < 10 ? ("0" + minutes.toString()) : minutes.toString());
				$(section).find(".seconds").text(seconds < 10 ? ("0" + seconds.toString()) : seconds.toString());
			}

			function setCurrentIncrement() {
				currentIncrement += 1;
				return currentIncrement;
			}
		}
	},

	hide_timer: function(frm) {
		frm.toolbar.page.inner_toolbar.find(".stopwatch").remove();
	},
	timer: function(frm) {
		return `<button> Start </button>`
	},

	set_total_completed_qty: function(frm) {
		frm.doc.total_completed_qty = 0;
		frm.doc.time_logs.forEach(d => {
			if (d.completed_qty) {
				frm.doc.total_completed_qty += d.completed_qty;
			}
		});

		refresh_field("total_completed_qty");
	}
});

frappe.ui.form.on('Job Card Time Log', {
	completed_qty: function(frm) {
		frm.events.set_total_completed_qty(frm);
	}
})



frappe.ui.form.on('Time Tracker', {
	setup(frm) {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(showPosition);
		  } else {
				frappe.msgprint("Geolocation is not supported by this browser.")
		  }
	}
});
function showPosition(position) {
	var msg = "Latitude: " + position.coords.latitude +
	"<br>Longitude: " + position.coords.longitude;
	//frappe.msgprint(msg)
  }
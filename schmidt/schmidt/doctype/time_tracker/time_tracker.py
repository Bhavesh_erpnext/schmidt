# -*- coding: utf-8 -*-
# Copyright (c) 2020, Bhavesh and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.utils import flt
import datetime
from datetime import timedelta
import json

json_location1 = {
	"type": "ClockIn",
	"features": [{
		"type": "Feature",
		"properties": {
			"point_type": "circle1",
			"radius": 300
		},
		"geometry": {
			"type": "Point",
			"coordinates": []
		}
	}]
}

class TimeTracker(Document):
	def before_insert(self):
		check_time_track = frappe.get_all("Time Tracker",filters=[[self.doctype,"project","=",self.project],[self.doctype,"employee","=",self.employee],[self.doctype,"name","!=",self.name]],fields=["*"])
		if len(check_time_track) >= 1:
			msg='Time Tracker Available For :'+'<a href="#Form/Time Tracker/'+check_time_track[0].name+'" class="c-msg">'+str(self.project)+'</a>'
			frappe.throw(msg)
	
	def validate(self):
		check_time_track = frappe.get_all("Time Tracker",filters=[[self.doctype,"employee","=",self.employee],[self.doctype,"name","!=",self.name],[self.doctype,"status","in",["Open","Work In Progress","On Hold"]]],fields=["*"])
		if len(check_time_track) >= 1:
			msg='You are already working on a Project :'+'<a href="#Form/Time Tracker/'+check_time_track[0].name+'" class="c-msg">'+str(self.project)+'</a>'
			frappe.throw(msg)
		if self.time_logs:
			for row in self.time_logs:
				format_string = '%Y-%m-%d %H:%M:%S'
				frappe.errprint(row.from_time)
				if row.from_time:
					from_time = datetime.datetime.strptime(row.from_time, format_string)
					row.from_time_format = from_time.strftime('%d-%m-%Y %H:%M')
				if row.to_time:
					to_time = datetime.datetime.strptime(row.to_time, format_string)
					row.to_time_format = to_time.strftime('%d-%m-%Y %H:%M')
				if row.time_in_mins:
					#frappe.errprint(str(timedelta(minutes=row.time_in_mins)).split(':'))
					time = str(timedelta(minutes=row.time_in_mins)).split(':')
					row.total_format = str(time[0])+':'+str(time[1])
				if not row.clockin_location:
					if row.clockin_latitude and row.clockin_longitude:
						#map_loc = [flt(row.clockin_latitude),flt(row.clockin_longitude)]
						#frappe.errprint(json_location)
						json_location = json_location1['features'][0]
						json_location['geometry']['coordinates'] = [flt(row.clockin_longitude),flt(row.clockin_latitude)]
						row.clockin_location = json.dumps(json_location)
				if not row.clockout_location:
					if row.clockout_latitude and row.clockout_longitude:
						#map_loc = [flt(row.clockout_latitude),flt(row.clockout_longitude)]
						#frappe.errprint(json_location)
						json_location = json_location1['features'][0]
						json_location['geometry']['coordinates'] = [flt(row.clockout_longitude),flt(row.clockout_latitude)]
						row.clockout_location = json.dumps(json_location)
		if self.total_billable_hours:
			total_hour = str(timedelta(minutes=self.total_time_in_mins)).split(':')
			self.total_hours = str(total_hour[0])+':'+str(total_hour[1])


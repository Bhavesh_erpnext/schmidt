# -*- coding: utf-8 -*-
# Copyright (c) 2020, Bhavesh and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
# import frappe
from frappe.model.document import Document

class Invoice(Document):
	def validate(self):
		self.calculate_total()

	def calculate_total(self):
		total = 0
		for item in self.invoice_item:
			item.amount = item.qty * item.rate
			total += item.amount
		self.net_amount = total
		if self.discount:
			self.grand_total = self.net_amount - self.discount
		else:
			self.grand_total = self.net_amount



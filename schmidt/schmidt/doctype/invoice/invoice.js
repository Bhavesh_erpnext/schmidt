// Copyright (c) 2020, Bhavesh and contributors
// For license information, please see license.txt

frappe.ui.form.on('Invoice', {
	refresh: function(frm) {

	},
	discount:function(frm,cdt,cdn){
		if(frm.doc.discount){
			console.log(frm.doc.discount)
			frappe.model.set_value(cdt,cdn,"grand_total",frm.doc.net_amount - frm.doc.discount)
		}
	}
});

frappe.ui.form.on('Invoice Item', {
	qty: function(frm,cdt,cdn) {
		calculate_item_total(frm,cdt,cdn)
	},
	rate:function(frm,cdt,cdn){
		calculate_item_total(frm,cdt,cdn)
	}
});


function calculate_item_total(frm,cdt,cdn){
	var doc = locals[cdt][cdn]
	if(doc.qty && doc.rate){
		frappe.model.set_value(cdt,cdn,"amount",doc.qty*doc.rate)
		var total = 0
		cur_frm.doc.invoice_item.forEach(d => {
			total += d.amount
		});
		cur_frm.set_value("net_amount",total)
		cur_frm.set_value("grand_total",parseFloat(total) - parseFloat(cur_frm.doc.discount))
	}
}
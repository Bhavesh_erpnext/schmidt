# -*- coding: utf-8 -*-
# Copyright (c) 2020, Bhavesh and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.utils import cstr, flt, cint, get_files_path

class EmployeeBill(Document):
	def validate(self):
		self.validate_overlap()
		get_jobs_calculate(self)


	def validate_overlap(self):
		bill = frappe.get_all("Employee Bill",filters=[["Employee Bill","employee","=",self.employee],["Employee Bill","from_date","=",self.from_date],["Employee Bill","to_date","=",self.to_date],["Employee Bill","name","!=",self.name]],fields=["name"])
		if bill:
			frappe.throw("Bill Already Created For Employee {0} Period {1} and {2}. Delete That And Create Again".format(self.employee,self.from_date,self.to_date))

@frappe.whitelist()
def get_jobs_calculate(self):
	data = frappe.db.sql("""select tt.project as 'project',sum(ttl.time_in_hour) as 'hour' from `tabTime Tracker` as tt inner join `tabJob Card Time Log` as ttl on tt.name=ttl.parent where tt.employee=%s and DATE(ttl.to_time) between %s and %s group by tt.project""",(self.employee,self.from_date,self.to_date),as_dict=1)
	self.items = []
	if data:
		total = 0
		for row in data:
			item = self.append("items",{})
			item.job_name = row.project
			item.total_hours = row.hour
			item.rate = frappe.db.get_value("Employee",self.employee,"hourly_rate")
			item.amount = flt(item.total_hours) * flt(item.rate)
			total += item.amount
		self.grand_total = total
 
// Copyright (c) 2020, Bhavesh and contributors
// For license information, please see license.txt

frappe.ui.form.on('Employee Payroll', {
	run_payroll: function(frm) {
		frm.trigger('generate_bill');
	},
	generate_bill:function(frm){
		frappe.call({
			method:"schmidt.schmidt.doctype.employee_payroll.employee_payroll.run_payroll",
			args:{'name':frm.doc.name},
			freeze:true,
			freeze_message:"Please Wait Bill Is Generating ..",
			callback:function(r){
				if(r.message == true){
					cur_frm.reload_doc();
				}
			}
		})
	}
});

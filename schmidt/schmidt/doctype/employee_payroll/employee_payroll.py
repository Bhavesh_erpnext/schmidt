# -*- coding: utf-8 -*-
# Copyright (c) 2020, Bhavesh and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class EmployeePayroll(Document):
	def autoname(self):
		self.name = 'Payroll' + '-' + str(self.from_date) + '-' + str(self.to_date)


@frappe.whitelist()
def run_payroll(name):
	payroll_doc = frappe.get_doc("Employee Payroll",name)
	employee_list = frappe.get_all("Employee",filters={"payroll_type":"On Payroll","status":"Active"},fields=["name"])
	total = 0
	if employee_list:
		for row in employee_list:
			res = create_bill(payroll_doc,row.name)
			if res == True:
				total += 1
	if total > 0:
		frappe.msgprint(str(total) + " Bill Created")
	return True


def create_bill(payroll_doc,emp):
	doc = frappe.get_doc(dict(
		doctype = "Employee Bill",
		employee = emp,
		posting_date = payroll_doc.posting_date,
		from_date = payroll_doc.from_date,
		to_date = payroll_doc.to_date
	)).insert(ignore_permissions = True)
	add_in_child(emp,doc.name,doc.grand_total,payroll_doc.name)
	if doc:
		return True


def add_in_child(employee,bill_no,grand_total,parent_name):
	doc = frappe.get_doc(dict(
		doctype = "Employee Payroll Bill",
		parentfield = "employee_payroll_bill",
		parenttype = "Employee Payroll",
		employee = employee,
		bill_no = bill_no,
		parent = parent_name,
		total = grand_total
	)).insert()


# Copyright (c) 2013, Bhavesh and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
from frappe import _
import frappe

def execute(filters=None):
	columns, data = [], []
	data = get_data(filters=filters)
	columns = get_columns(filters=filters)
	return columns, data

def get_columns(filters=None):
	return [
		_("Job") + ":Data:220",
		_("Hour") + ":Float:100"
	]

def get_data(filters=None):
	if frappe.session.user == "Administrator":
		return frappe.db.sql("""select concat("<a href=#Form/Time%20Tracker/",name,">",project,"</a>") as 'project',total_billable_hours from `tabTime Tracker` where project in (select name from `tabJob`)""")
	manager_user = frappe.db.sql_list("""select distinct parent from `tabHas Role` where role in ('Projects Manager','System Manager','Accounts Manager','Schmidt Manager') and parent=%s""",frappe.session.user)
	if len(manager_user) >= 1:
		return frappe.db.sql("""select concat("<a href=#Form/Time%20Tracker/",name,">",project,"</a>") as 'project',total_billable_hours from `tabTime Tracker` where project in (select name from `tabJob`)""")
	staff_user = frappe.db.sql_list("""select distinct parent from `tabHas Role` where role='Employee'and parent=%s""",frappe.session.user)
	if len(staff_user) >= 1:
		return frappe.db.sql("""select concat("<a href=#Form/{0}/",name,">",project,"</a>") as 'project',total_billable_hours from `tabTime Tracker` where owner='{1}'""".format('Time%20Tracker',frappe.session.user))

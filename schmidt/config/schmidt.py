from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
	{
			"label": _("Estimate"),
			"items": [
				{
					"type": "doctype",
					"name": "Exterior Estimate",
					"onboard": 0
				},
				{
					"type": "doctype",
					"name": "Interior Estimate",
					"onboard": 0
				},
				{
					"type": "doctype",
					"name": "Power Wash Estimate",
					"onboard": 0
				},
				{
					"type": "doctype",
					"name": "Web Form",
					"onboard": 0
				},
				{
					"type": "doctype",
					"name": "DocType",
					"onboard": 0
				}
			]
		},
	{
			"label": _("Tracker"),
			"items": [
				{
					"type": "report",
					"name": "Time Tracker",
					"doctype":"Time Tracker",
					"is_query_report": True
				}

			]
	},
	{
			"label": _("Project"),
			"items": [
				{
					"type": "doctype",
					"name": "Schmidt Project",
					"onboard": 0
				},
				{
					"type": "doctype",
					"name": "Job",
					"onboard": 0
				}
			]
	},
	{
			"label": _("Invoice"),
			"items": [
				{
					"type": "doctype",
					"name": "Client",
					"onboard": 0
				},
				{
					"type": "doctype",
					"name": "Invoice",
					"onboard": 0
				},
				{
					"type": "doctype",
					"name": "Job Item",
					"onboard": 0
				}
			]
	}

]
